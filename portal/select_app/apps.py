from django.apps import AppConfig


class SelectAppConfig(AppConfig):
    name = 'select_app'
