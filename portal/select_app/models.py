from django.db import models


class Country(models.Model):
    country_name = models.CharField(max_length=150, unique=True, verbose_name='Country')
    country_code = models.CharField(max_length=2, unique=True, verbose_name='Code')

    def __str__(self):
        return self.country_name

    class Meta:
        verbose_name = 'Country'
        verbose_name_plural = 'Countries'
        ordering = ['pk']


class City(models.Model):
    city_id_code = models.IntegerField(unique=True, verbose_name="ID code")
    city_name = models.CharField(max_length=150, verbose_name="City")
    country = models.ForeignKey(Country, on_delete=models.PROTECT, verbose_name='Country')
    state_code = models.CharField(max_length=2, null=True)

    def __str__(self):
        if self.state_code:
            return self.city_name + '  (' + self.country.country_name + ' / ' + self.state_code + ')'
        return self.city_name + '  (' + self.country.country_name + ')'

    class Meta:
        verbose_name = 'City'
        verbose_name_plural = 'Cities'
        ordering = ['pk']


class CityAuto(models.Model):
    city = models.ForeignKey(City, on_delete=models.PROTECT, verbose_name='City AUTOCOMPLETE')


class Weather(models.Model):
    code = models.IntegerField(unique=True)
    description = models.CharField(max_length=100)
    icon_type = models.CharField(max_length=10)

    def __str__(self):
        return self.description

    class Meta:
        verbose_name = 'Weather Condition'
        verbose_name_plural = 'Weather Conditions'
        ordering = ['pk']


class WeatherStamp(models.Model):
    city = models.ForeignKey(City, on_delete=models.CASCADE)
    weather = models.ForeignKey(Weather, on_delete=models.CASCADE)
    temp = models.FloatField(null=True)
    pressure = models.IntegerField(null=True)
    humidity = models.IntegerField(null=True)
    visibility = models.IntegerField(null=True)
    wind_speed = models.FloatField(null=True)
    created_on = models.DateField(auto_now_add=True)

    class Meta:
        verbose_name = 'Weather Stamp'
        verbose_name_plural = 'Weather Stamps'
        ordering = ['-created_on']
        ordering = ['-pk']
