from django.contrib import admin
from .models import Country, City, CityAuto, Weather, WeatherStamp


class CountryAdmin(admin.ModelAdmin):
    list_display = ('country_name', 'country_code')
    list_display_links = ('country_name',)
    search_fields = ('country_name', 'country_code')


class CityAdmin(admin.ModelAdmin):
    list_display = ('city_id_code', 'city_name', 'country')
    list_display_links = ('city_name',)
    search_fields = ('city_name',)


# class CityAutoAdmin(admin.ModelAdmin):
#     list_display = ('city',)
#     list_display_links = ('city',)
#     search_fields = ('city_name',)


class WeatherAdmin(admin.ModelAdmin):
    list_display = ('code', 'description', 'icon_type')
    list_display_links = ('code', 'description')
    search_fields = ('description',)


class WeatherStampAdmin(admin.ModelAdmin):
    list_display = ('created_on',
                    'city',
                    'temp',
                    'pressure',
                    'humidity',
                    'weather',
                    'visibility',
                    'wind_speed'
                    )
    list_display_links = ('city',)
    search_fields = ('city',)


admin.site.register(Country, CountryAdmin)
admin.site.register(City, CityAdmin)
# admin.site.register(CityAuto, CityAutoAdmin)
admin.site.register(Weather, WeatherAdmin)
admin.site.register(WeatherStamp, WeatherStampAdmin)

