from django.urls import path
from .views import SelectView, InputFormView, SelectFormView, AutocompleteFormView

urlpatterns = [
    path('', SelectView.as_view(), name='select'),

    path('select/input/', InputFormView.as_view(), name='select_input'),
    path('select/select/', SelectFormView.as_view(), name='select_select'),
    path('select/autocomplete/', AutocompleteFormView.as_view(), name='autocomplete'),

]
