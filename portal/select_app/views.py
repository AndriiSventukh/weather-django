from django.http import JsonResponse
from django.shortcuts import render, redirect
from django.views import View
from .models import Country, City, Weather, WeatherStamp
from .forms import CityInputForm, CitySelectForm, CityAutocompleteForm
from .initial_db_filler import check_db_for_data_existence
import requests
from django.contrib import messages
from portal.settings import API_KEY



class SelectView(View):
    """
    Creates view for rendering available forms
    """

    def get(self, request):
        """
        Acquire GET request, check whether the DB contains data of cities with available
        weather information, makes initial fill up and render html-page
        :param request:
        :return: render html-page with provided context
        """
        check_db_for_data_existence()
        form_input = CityInputForm()
        form_select = CitySelectForm()
        form_autocomplete = CityAutocompleteForm()
        context = {
            'form_input': form_input,
            'form_select': form_select,
            'form_autocomplete': form_autocomplete,
        }
        return render(request, 'select_app/index.html', context=context)


class InputFormView(View):
    """
    Creates view for perceiving data from text input form
    """
    def post(self, request):
        form_input = CityInputForm(request.POST)

        if form_input.is_valid():
            name = form_input.cleaned_data['city_name']
            response = requests.get(f'https://api.openweathermap.org/data/2.5/weather?q={name}&appid={API_KEY}')

            if response.status_code == 200:
                try:
                    response_obj = response.json()
                    city = City.objects.filter(city_id_code=int(response_obj.get('id'))).first()
                    weather = Weather.objects.get(code=response_obj.get('weather')[0].get('id'))
                    if WeatherStamp.objects.create(city=city,
                                                   weather=weather,
                                                   temp=response_obj.get('main').get('temp'),
                                                   pressure=response_obj.get('main').get('pressure'),
                                                   humidity=response_obj.get('main').get('humidity'),
                                                   visibility=response_obj.get('visibility'),
                                                   wind_speed=response_obj.get('wind').get('speed'),
                                                   ):
                        messages.success(request, 'Weather stamp successfully added to DB.')
                except Exception as e:
                    messages.error(request, f"""ServerError! Weather stamp for "{name}" can't be saved!""")
                    return redirect('select')
            else:
                messages.warning(request, f"""Can't obtain info about "{name}"!""")

        else:
            context = {
                'form_input': form_input,
                'form_select': CitySelectForm(),
                'form_autocomplete': CityAutocompleteForm()

            }
            return render(request, 'select_app/index.html', context=context)
        return redirect('select')


class SelectFormView(View):
    """
    Creates view for perceiving data from select form
    """
    def post(self, request):
        form_select = CitySelectForm(request.POST)
        if form_select.is_valid():
            city = form_select.cleaned_data['city']
            name = city.city_name
            _id = city.city_id_code
            response = requests.get(f'https://api.openweathermap.org/data/2.5/weather?id={_id}&appid={API_KEY}')

            if response.status_code == 200:
                try:
                    response_obj = response.json()
                    weather = Weather.objects.get(code=response_obj.get('weather')[0].get('id'))
                    if WeatherStamp.objects.create(city=city,
                                                   weather=weather,
                                                   temp=response_obj.get('main').get('temp'),
                                                   pressure=response_obj.get('main').get('pressure'),
                                                   humidity=response_obj.get('main').get('humidity'),
                                                   visibility=response_obj.get('visibility'),
                                                   wind_speed=response_obj.get('wind').get('speed'),
                                                   ):
                        messages.success(request, 'Weather stamp successfully added to DB.')
                except Exception as e:
                    messages.error(request, f"""ServerError! Weather stamp for "{name}" can't be saved!""")
                    return redirect('select')
            else:
                messages.warning(request, f"""Can't obtain info about "{name}"!""")

        else:
            context = {
                'form_input': CityInputForm(),
                'form_select': form_select,
                'form_autocomplete': CityAutocompleteForm(),
            }
            return render(request, 'select_app/index.html', context=context)

        return redirect('select')


class AutocompleteFormView(View):
    """
    Creates view for perceiving data from autocomplete form
    """
    def get(self, request):
        if request.is_ajax():
            term = request.GET.get('term')
            cities = City.objects.filter(city_name__istartswith=term)
            response = [{'id': city.id, 'title': str(city)} for city in cities]
            return JsonResponse(response, safe=False)

    def post(self, request):
        if request.POST.get('city'):
            city = City.objects.get(id=request.POST.get('city'))
            _id = city.city_id_code
            name = city.city_name
            response = requests.get(f'https://api.openweathermap.org/data/2.5/weather?id={_id}&appid={API_KEY}')
            if response.status_code == 200:
                try:
                    response_obj = response.json()
                    weather = Weather.objects.get(code=response_obj.get('weather')[0].get('id'))
                    if WeatherStamp.objects.create(city=city,
                                                   weather=weather,
                                                   temp=response_obj.get('main').get('temp'),
                                                   pressure=response_obj.get('main').get('pressure'),
                                                   humidity=response_obj.get('main').get('humidity'),
                                                   visibility=response_obj.get('visibility'),
                                                   wind_speed=response_obj.get('wind').get('speed'),
                                                   ):
                        messages.success(request, 'Weather stamp successfully added to DB.')
                except Exception as e:
                    messages.error(request, f"""ServerError! Weather stamp for "{name}" can't be saved!""")
                    return redirect('select')
            else:
                messages.warning(request, f"""Can't obtain info about "{name}"!""")

        else:
            context = {
                'form_input': CityInputForm(),
                'form_select': CitySelectForm(),
                'form_autocomplete': CityAutocompleteForm(),
            }
            return render(request, 'select_app/index.html', context=context)
        return redirect('select')

