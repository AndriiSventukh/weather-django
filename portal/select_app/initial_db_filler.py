import os.path
import csv
import json
from select_app.models import Country, City, Weather
from .errors import DBFillerError


def fill_country_table():
    """
    Fill db_table with data of existing countries from *.json file
    :param: None
    :return: None
    """
    p = os.path.abspath('portal/src/data/country_codes.json')
    with open(p, 'r', encoding="utf-8") as json_f:
        countries = json.loads(json_f.read())
    if countries:
        for country in countries:
            try:
                Country.objects.create(country_name=country.get('name'),
                                       country_code=country.get('alpha-2'))
            except DBFillerError as e:
                print(e.args)


def fill_city_table():
    """
    Fill db_table with data of existing cities from *.json file
    :param: None
    :return: None
    """
    p = os.path.abspath('portal/src/data/city_list.json')
    with open(p, 'r', encoding="utf-8") as json_f:
        cities = json.loads(json_f.read())
    if cities:
        for city in cities:
            if city.get('country'):
                try:
                    City.objects.create(city_id_code=city.get('id'),
                                        city_name=city.get('name'),
                                        country=Country.objects.get(country_code=city.get('country')),
                                        state_code=city.get('state')
                                        )
                except DBFillerError as e:
                    print(e.args)


def fill_weather_table():
    """
    Fill db_table with data of existing weather conditions from *.csv file
    :param: None
    :return: None
    """
    p = os.path.abspath('portal/src/data/weather_codes.csv')
    with open(p, 'r') as csv_f:
        weather_conditions = csv.reader(csv_f)
        for weather_condition in weather_conditions:
            try:
                Weather.objects.create(code=int(weather_condition[0]),
                                       description=weather_condition[2],
                                       icon_type=weather_condition[3]
                                       )
            except DBFillerError as e:
                print(e.args)


def check_db_for_data_existence():
    """
    Check whether the according tables have data and if not, fill them
    :param: None
    :return: None
    """
    if not Country.objects.exists():
        fill_country_table()

    if not City.objects.exists():
        fill_city_table()

    if not Weather.objects.exists():
        fill_weather_table()
