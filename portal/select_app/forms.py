from django import forms
from .models import City, CityAuto
from django.db.models import Count


class CityInputForm(forms.Form):
    """
    Text input form for name of city
    """
    city_name = forms.CharField(max_length=150,
                                label='CITY INPUT',
                                widget=forms.TextInput(attrs={'class': 'form-control',
                                                              'placeholder': 'Input city name in English:'}))


class CitySelectForm(forms.Form):
    """
    Select form for choosing city from already existing in DB cities
    """
    city = forms.ModelChoiceField(empty_label='- Choose the city from DB -',
                                  queryset=City.objects.annotate(cnt=Count('weatherstamp')).filter(cnt__gt=0).order_by('city_name'),
                                  label='CITY SELECT',
                                  widget=forms.Select(attrs={'class': 'form-select'}))


class CityAutocompleteForm(forms.ModelForm):
    """
    Autocomplete form for handling full list of cities
    """
    class Meta:
        model = CityAuto
        fields = ('city',)
        labels = ('City AUTOCOMPLETE',)
        widgets = {
            'city': forms.Select(attrs={'id': 'id_city2',
                                        'class': 'form-select',
                                        'data-placeholder': 'Input city name in English:'})
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['city'].queryset = City.objects.annotate(cnt=Count('weatherstamp')).filter(cnt__gt=0)
        """
        needed in case if select2 service unavailable
        """
        for city in self.fields.keys():
            self.fields[city].widget.attrs.update({
                'class': 'form-control',
            })
