class DBFillerError(Exception):
    """
    Makes specific exception for convenient exceptions handling while initially filling DB
    """
    pass
