from django.urls import path
from .views import API_View

urlpatterns = [
    path('telegram/', API_View.as_view(), name='telegram_api'),
]
