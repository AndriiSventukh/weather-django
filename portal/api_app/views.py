from django.views.decorators.csrf import csrf_exempt
from django.utils.decorators import method_decorator
from django.views import View
from select_app.models import Weather
import json
import requests
from django.http import HttpResponse
from portal.settings import API_KEY, BOT_TOKEN


@method_decorator(csrf_exempt, name='dispatch')
class API_View(View):
    def post(self, request):

        req_data = json.loads(request.body.decode())
        name = req_data.get('message').get('text')
        chat_id = req_data.get('message').get('chat').get('id')

        if name != '/start':
            response = requests.get(f'https://api.openweathermap.org/data/2.5/weather?q={name}&appid={API_KEY}')

            if response.status_code == 200:
                response_obj = response.json()
                city_name = response_obj.get('name')
                weather = Weather.objects.get(code=response_obj.get('weather')[0].get('id')).description
                temperature = "%.1f" % (float(response_obj.get('main').get('temp'))-273.15)
                pressure = response_obj.get('main').get('pressure')
                humidity = response_obj.get('main').get('humidity')
                visibility = response_obj.get('visibility')
                wind_speed = response_obj.get('wind').get('speed')
                degree_sign = u'\xb0'
                response_str = f"Location: {city_name}\n" \
                               f"  temperature {temperature} {degree_sign}C\n" \
                               f"  {weather}\n" \
                               f"  pressure {pressure}\n" \
                               f"  humidity {humidity}%\n" \
                               f"  wind {wind_speed}m/c\n" \
                               f"  visibility {visibility}m"
                requests.get(f'https://api.telegram.org/bot{BOT_TOKEN}/sendMessage?chat_id={chat_id}&text={response_str}')
            else:
                requests.get(f"https://api.telegram.org/bot{BOT_TOKEN}/sendMessage?chat_id={chat_id}&text=Information about \'{name}\' can\'t be obtained by current service")

        return HttpResponse(status=200)

