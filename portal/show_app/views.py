from django.shortcuts import render
from django.views import View
from select_app.models import WeatherStamp
from django.core.paginator import Paginator
from .forms import FilterForm


class ShowView(View):
    """
    Creates view for rendering data in general and filtered form
    """
    def get(self, request):
        filter_form = FilterForm(request.GET)
        form_data = {
                    'city': '',
                    'date_from': '',
                    'date_till': ''
                }
        query = ["SELECT * FROM select_app_weatherstamp"]
        params = []

        if filter_form.is_valid():
            if filter_form.cleaned_data['city'] or \
                    filter_form.cleaned_data['date_from'] or \
                    filter_form.cleaned_data['date_till']:
                query.append(' WHERE')
                if filter_form.cleaned_data['city']:
                    query.append(' city_id = %s')
                    params.append(filter_form.cleaned_data['city'].id)
                    form_data['city'] = filter_form.cleaned_data['city'].id
                if filter_form.cleaned_data['date_from']:
                    if len(query) >= 3:
                        query.append(' and')
                    query.append(' created_on >= %s')
                    params.append(filter_form.cleaned_data['date_from'])
                    form_data['date_from'] = filter_form.cleaned_data['date_from']
                if filter_form.cleaned_data['date_till']:
                    if len(query) >= 3:
                        query.append(' and')
                    query.append(' created_on <= %s')
                    params.append(filter_form.cleaned_data['date_till'])
                    form_data['date_till'] = filter_form.cleaned_data['date_till']
        query.append(' ORDER BY created_on DESC')
        query = "".join(query)
        WStamps = WeatherStamp.objects.raw(query, params)

        form_data = ''.join(['&'+str(key)+'='+str(value) for key, value in form_data.items()])

        wstamps_list = []
        for wstamp in WStamps:
            icons = wstamp.weather.icon_type.split('.')
            wstamps_list.append({'stamp': wstamp, 'icons': icons})

        page_number = request.GET.get('page', 1)
        paginator = Paginator(wstamps_list, 7)
        page = paginator.get_page(page_number)

        has_another = page.has_other_pages()
        prev_url = f'?page={page.previous_page_number()}{form_data}' if page.has_previous() else ''
        next_url = f'?page={page.next_page_number()}{form_data}' if page.has_next() else ''

        context = {
            'wpage': page,
            'is_paginated': has_another,
            'prev_url': prev_url,
            'next_url': next_url,
            'filter_form': filter_form,
            'form_data': form_data
        }

        return render(request, 'show_app/show.html', context=context)
