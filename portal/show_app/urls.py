from django.urls import path
from .views import ShowView

urlpatterns = [
    path('show/', ShowView.as_view(), name='show'),
]
