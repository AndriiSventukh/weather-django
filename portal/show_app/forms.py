from django import forms
from select_app.models import City
from django.db.models import Count


class FilterForm(forms.Form):
    """
    Form for filtering data concerning weatherstamps
    """
    city = forms.ModelChoiceField(empty_label='- Choose the city from DB -',
                                  required=False,
                                  queryset=City.objects.annotate(cnt=Count('weatherstamp')).filter(cnt__gt=0).order_by('city_name'),
                                  label='CITY SELECT',
                                  widget=forms.Select(attrs={'class': 'form-select'}),
                                  initial=None)

    date_from = forms.DateField(required=False,
                                label='From',
                                widget=forms.DateInput(attrs={'type': 'date', 'class': 'form-control'}),
                                initial=None)

    date_till = forms.DateField(required=False,
                                label='Till',
                                widget=forms.DateInput(attrs={'type': 'date', 'class': 'form-control'}),
                                initial=None)
