# Weather Portal (Django)

Current app gives an opportunity to obtain current weather conditions for a wide  
variety of cities in different countries using third-party source - [Openweather](https://openweathermap.org/api).  
It was deployed on Heroku Cloud Application Platform in a shortened variant*  ([Link](https://findweatherbycityname.herokuapp.com)).  
(due to some constraints, Heroku offers only 10000 raws (for free) for DB instead  
of more than 210 000 nedeed in the current app.  

It was taken into account that it is faster to check possibility to get some info  
from remote servise previousli just looking into existing DB, containing the full  
list of cities (the cities that we can get information from [Openweather](https://openweathermap.org/api) for).  
So, according to this and some other reasons the appropriate tables with information  
about cities, countries, weather conditions params and consequently about weather  
conditions stamps for certain date were created in DB. Initial filling was made by  
meas of parsing info from appropriate *.json and *.csv files.  

### App contains two pages.  

On the first one user can get info about weather for city, he is interested in:  
![Image](img_readme/1.png)  

Here user can choose different ways of geting data: via text input (1), via select  
field with list of previously checked cities (2) and via using autocomplete search  
field (5). Here also source code can be referenced (4).  
It wasn't possible to add almost 210 000 cities from available list to select field,  
so the only way to implement the possibility to select city from that list is to make  
autocomplete search field:  
![Image](img_readme/2.png)  

On the second page user can look at the saved in the DB info about weather conditions.  
![Image](img_readme/3.png)  
*messages about errors and success are available  

On this page user can filter existing data by cities and dates ither simultaneously by  
several criteria or separately (depends on goals).  
![Image](img_readme/4.png)  
  
*in case of huge amount of filtered data pagination is also available.  
** filtering realized using raw SQL queries.  
  
### Also Telegram-bot [@WeatherForLocation](https://t.me/weatherforcity_bot) available for current app.  
It allows to enquire for weather conditions all over the world just entering the city or  
location name.  
![Image](img_readme/5.png)  
